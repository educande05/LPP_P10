lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "Alimento/version"

Gem::Specification.new do |spec|
  spec.name          = "alimentos"
  spec.version       = Alimento::VERSION
  spec.authors       = ["Eduardo Pérez Suárez"]
  spec.email         = ["alu0100890174@ull.edu.es"]

  spec.summary       = %q{Gema para la practica 6 de alimentos.}
  spec.description   = %q{Esta práctica sirve para comprobar las funcionalidades de spec y trabajar con clases en ryby}
  spec.homepage      = "https://github.com/ULL-ESIT-LPP-1920/tdd-educande05.git"
  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://github.com/ULL-ESIT-LPP-1819/tdd-alu0101061672"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end

  spec.rdoc_options += ['-m', 'README.rdoc', '-x', 'lib/(?!myclass.rb).*',
                     'lib/myclass.rb', 'LICENSE', 'README.rdoc']
  spec.extra_rdoc_files = []

  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.9"

  spec.add_development_dependency "guard"
  spec.add_development_dependency "guard-rspec"
  spec.add_development_dependency "guard-bundler"
end
